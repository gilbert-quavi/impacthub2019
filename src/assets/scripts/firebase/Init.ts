import Users from "./Users"
import ListingItems from "../firebends/ListingItems"
import { ListingItem, ListingType } from "../model/app/ListingItem"

function init() {
  // init test data
  const items: ListingItem[] = [
    {
      title: 'Premium Local Rice (white)',
      expiration: new Date().getTime(),
      weight: 1000,
      price: 40000,
      date: new Date('Feb 14, 2019').getTime(),
      listingType: ListingType.BUYING,
      details: ['Refined, well-milled, soft and aromatic'],
      userId: 'OUrwvOtmKoSMxnLY2AR3HvhO0DG2',
      imgUrl: 'TODO'
    },

    {
      title: 'Regular Well-milled Rice (white)',
      expiration: new Date().getTime(),
      weight: 1000,
      price: 36000,
      date: new Date('Feb 14, 2019').getTime(),
      listingType: ListingType.BUYING,
      details: ['Long-grain, well-milled'],
      userId: 'OUrwvOtmKoSMxnLY2AR3HvhO0DG2',
      imgUrl: 'TODO'
    },

    {
      title: 'Well-milled Local Rice (white)',
      expiration: new Date().getTime(),
      weight: 1000,
      price: 38000,
      date: new Date('Feb 14, 2019').getTime(),
      listingType: ListingType.BUYING,
      details: ['Well-milled'],
      userId: 'OUrwvOtmKoSMxnLY2AR3HvhO0DG2',
      imgUrl: 'TODO'
    }
  ]
  items.forEach(item => {
    console.log(item)
    ListingItems.add(item)
  })
}

export default {
  init
}